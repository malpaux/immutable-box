/**
 * @file Master entrypoint
 */

/**
 * @module immutable-box
 * @license Copyright (c) 2017 Malpaux IoT All Rights Reserved.
 */

// @flow

/**
 * Check two values for equality
 * @private
 * @param {*} value1 - The first value
 * @param {*} value2 - The second value
 * @return {bool}
 */
const is = (value1: *, value2: *): bool =>
  value1 === value2;

/**
 * The box' identifying properties' name
 * @private
 */
const IS_BOX_SENTINEL = '@@__IMMUTABLE_BOX__@@';

/**
 * Immutable Box
 * @alias module:immutable-box
 */
// $FlowFixMe: Whatever?
class Box<T> {
  size: 1 | 0;
  _value: ?T;
  __ownerID: ?bool;
  __altered: bool;

  // @pragma Construction

  /**
   * Create a new box
   * @param {*} value - The box' value
   * @param {?bool} mutable - Should the box be mutable?
   * @return {Box} The box
   */
  constructor(value: ?T, mutable: ?bool): Box<T> {
    /* if (Box.isBox(value)) {
      return value;
    } */

    return value === undefined && !mutable ?
      emptyBox() // eslint-disable-line no-use-before-define
    : makeBox(value, mutable); // eslint-disable-line no-use-before-define
  }

  /**
   * Check if the given value is a box
   * @param {*} maybeBox - The value to check
   * @return {bool}
   */
  static isBox(maybeBox: ?Box<T>) {
    // $FlowFixMe: IS_BOX_SENTINEL is applied manually
    return !!(maybeBox && maybeBox[IS_BOX_SENTINEL]);
  }

  // @pragma Conversion

  /**
   * Convert the box to an array
   * @return {Array} The array
   */
  toArray(): Array<?T> {
    return this.isEmpty() ? [] : [this._value];
  }

  /**
   * Convert the box to JS
   * @return {*} The box' value
   */
  toJS(): ?T {
    return this._value;
  }

  /**
   * Convert the box to JS
   * @return {*} The box' value
   */
  toJSON(): ?T {
    return this._value;
  }

  /**
   * Make the box mutable
   * @return {Box} The box
   */
  asMutable(): Box<T> {
    return makeBox(this._value, true); // eslint-disable-line no-use-before-define
  }

  /**
   * Make the box immutable
   * @return {Box} The box
   */
  asImmutable(): Box<T> {
    return makeBox(this._value); // eslint-disable-line no-use-before-define
  }

  /**
   * Apply the given mutator, return an immutable box
   * @param {Function} fn - The function
   * @return {Box} The box
   */
  withMutations(fn: (Box<T>) => Box<T>): Box<T> {
    if (typeof fn === 'function') {
      const maybeBox = fn(this.asMutable());
      if (Box.isBox(maybeBox)) return maybeBox.asImmutable();
    }
    return this;
  }

  // @pragma Access

  /**
   * Check the box for equality
   * @param {Box} value - The box to check against
   * @return {bool}
   */
  equals(value: ?Box<T>): bool {
    return !!(value && Box.isBox(value) && is(this._value, value.get()));
  }

  /**
   * Get the box' value
   * @return {*} The value
   */
  get(): ?T {
    return this._value;
  }

  /**
   * Check if the box' includes a value
   * @param {*} value - The value to check against
   * @return {bool}
   */
  includes(value: ?T): bool {
    return is(this._value, value);
  }

  // @pragma Modification

  /**
   * Set the box' value
   * @param {*} value - The new value
   * @return {Box} The box
   */
  set(value: ?T): Box<T> {
    return updateBox(this, value); // eslint-disable-line no-use-before-define
  }

  /**
   * Clear the box
   * @return {Box} The box
   */
  clear(): Box<T> {
    if (this.__ownerID) {
      this._value = undefined;
      this.__altered = true;
      return this;
    }
    return emptyBox(); // eslint-disable-line no-use-before-define
  }

  /**
   * Update the box' value (map)
   * @param {Function} fn - The updater (mapper) function
   * @return {Box} The box
   */
  map(fn: (?T) => ?T): Box<T> {
    return (typeof fn === 'function') ?
      this.set(fn(this.get()))
    : this;
  }

  /**
   * Update the box (pipe to function)
   * @param {Function} fn - The updater function
   * @return {Box} The box
   */
  update(fn: (Box<T>) => Box<T>): Box<T> {
    return (typeof fn === 'function') ?
      fn(this)
    : this;
  }

  /**
   * Filter the box' value
   * @param {Function} fn - The qualifying function
   * @return {Box} The box
   */
  filter(fn: (?T) => ?bool): Box<T> {
    return (typeof fn === 'function') ?
      this.map((value) => (fn(value) ? value : undefined))
    : this;
  }

  /**
   * Filter the box' value
   * @param {Function} fn - The qualifying function
   * @return {Box} The box
   */
  filterNot(fn: (?T) => ?bool): Box<T> {
    return (typeof fn === 'function') ?
      this.filter((value) => !fn(value))
    : this;
  }

  // @pragma Side Effects

  /**
   * Pass the box' value to a function
   * @param {Function} fn
   * @return {Box} The box
   */
  forEach(fn: (?T) => void): Box<T> {
    if (typeof fn === 'function') fn(this._value);
    return this;
  }

  // @pragma Reduction

  /**
   * Check if the box is empty
   * @return {bool}
   */
  isEmpty() {
    return this._value === undefined;
  }
}

/**
 * The box' prototype
 * @private
 */
const BoxPrototype: Object = Box.prototype;
BoxPrototype[IS_BOX_SENTINEL] = true;

/**
 * Create a new box
 * @private
 * @param {*} value - The box' value
 * @param {} ownerID - The ownerID
 * @return {Box} - The new box
 */
const makeBox = <T>(value: ?T, ownerID: ?bool): Box<T> => {
  const box: Box<T> = Object.create(BoxPrototype);
  /**
   * The box' size
   * @public
   * @alias module:immutable-box#size
   */
  box.size = value !== undefined ? 1 : 0;
  box._value = value;
  box.__ownerID = ownerID;
  box.__altered = false;
  return box;
};

/**
 * An empty box
 * @private
 */
let EMPTY_BOX: ?Box<*>;

/**
 * Create an empty box
 * @private
 * @return {Box}
 */
const emptyBox = (): Box<*> =>
  EMPTY_BOX || (EMPTY_BOX = makeBox());

/**
 * Update a box
 * @private
 * @param {Box} box - The box
 * @param {*} value - The box' new value
 * @return {Box} The updated box
 */
const updateBox = <T>(box: Box<T>, value: ?T): Box<T> => {
  const _box: Box<T> = box;
  if (_box.__ownerID) {
    _box._value = value;
    _box.__altered = true;
    return _box;
  }
  return makeBox(value);
};

// Enable box creation w/o 'new' keyword
const boxConstructor = <T>(v: ?T, m: ?bool): Box<T> => new Box(v, m);
boxConstructor.isBox = Box.isBox;
module.exports = boxConstructor;
