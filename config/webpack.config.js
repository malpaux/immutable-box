var webpack = require('webpack'),
    paths = require('./paths'),
    fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

var env = process.env.NODE_ENV;
var config = {
  bail: env === 'production',
  target: "node",
  devtool: env === 'development' ? 'source-map' : undefined,

  entry: paths.appJs,
  output: {
    library: paths.appName,
    libraryTarget: "commonjs2",
    path: paths.appBuild,
    filename: '[name].js'
  },

  module: {
    loaders: [
      {
        enforce: "pre",
        test: /\.(js|jsx)$/,
        loader: 'eslint-loader',
        include: paths.appSrc,
        exclude: /\.min./
      },
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        include: paths.appSrc
      }
    ]
  },

  externals: nodeModules,

  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(env)
    })
  ]
};

if (env === 'production') {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        pure_getters: true,
        unsafe: true,
        unsafe_comps: true,
        warnings: false
      },
      mangle: true,
      output: {
        comments: false
      }
    })
  );
}

module.exports = config;
