/**
 * @file Test suite
 * @license Copyright (c) 2017 Malpaux IoT All Rights Reserved.
 */

import Box from './';

describe('immutable box', () => {
  it('should create a new box', () => {
    expect(new Box());
    expect(new Box('test'));
    expect(Box());
  });

  it('should identify a box', () => {
    // Test for false-positives
    expect(Box.isBox()).toBe(false);
    expect(Box.isBox([])).toBe(false);
    expect(Box.isBox('test')).toBe(false);

    // Test identification
    expect(Box.isBox(new Box())).toBe(true);
    expect(Box.isBox(new Box(1))).toBe(true);
  });

  it('should convert the box to an array', () => {
    expect((new Box()).toArray()).toEqual([]);
    expect((new Box(0)).toArray()).toEqual([0]);
    expect((new Box('test')).toArray()).toEqual(['test']);
  });

  it('should convert the box to JS', () => {
    expect((new Box()).toJS()).toBe();
    expect((new Box(0)).toJS()).toBe(0);
    expect((new Box('test')).toJS()).toBe('test');
  });

  it('should convert the box to JSON', () => {
    expect((new Box()).toJSON()).toBe();
    expect((new Box(0)).toJSON()).toBe(0);
    expect((new Box('test')).toJSON()).toBe('test');
  });

  it('should check the box for equality', () => {
    expect((new Box()).equals(new Box())).toBe(true);
    expect((new Box(0)).equals(new Box(0))).toBe(true);
    expect((new Box('test')).equals(new Box('test'))).toBe(true);

    expect((new Box('test')).equals(new Box('not test'))).toBe(false);
  });

  it('should get the box\' value', () => {
    expect((new Box()).get()).toBe();
    expect((new Box(0)).get()).toBe(0);
    expect((new Box('test')).get()).toBe('test');
  });

  it('should check if the box\' includes a value', () => {
    expect((new Box()).includes()).toBe(true);
    expect((new Box(0)).includes(0)).toBe(true);
    expect((new Box('test')).includes('test')).toBe(true);

    expect((new Box('test')).includes('not test')).toBe(false);
  });

  it('should set the box\' value', () => {
    const box = new Box(0);
    expect(box.set(1).toJS()).toBe(1);
    expect(box.toJS()).toBe(0);
  });

  it('should clear the box', () => {
    const box = new Box('test');
    expect(box.clear().toJS()).toBe();
    expect(box.toJS()).toBe('test');
  });

  it('should map the box', () => {
    const mock = (jest.fn()).mockReturnValue(1);
    expect(new Box(0).map(mock).toJS()).toBe(1);
    expect(mock).toHaveBeenCalledTimes(1);
    expect(mock).toHaveBeenCalledWith(0);
  });

  it('should update (pipe) the box', () => {
    const mock = (jest.fn()).mockReturnValue(new Box(1));
    expect(new Box(0).update(mock).toJS()).toBe(1);
    expect(mock).toHaveBeenCalledTimes(1);
    expect(mock.mock.calls[0][0].get()).toBe(0);
  });

  it('should filter the box', () => {
    const mock = (jest.fn()).mockReturnValue(false);
    expect(new Box(0).filter(mock).toJS()).toBe();
    expect(mock).toHaveBeenCalledTimes(1);
    expect(mock).toHaveBeenCalledWith(0);

    mock.mockReturnValue(true);
    expect(new Box(1).filter(mock).toJS()).toBe(1);
    expect(mock).toHaveBeenCalledTimes(2);
    expect(mock).toHaveBeenLastCalledWith(1);
  });

  it('should filter the box invertedly', () => {
    const mock = (jest.fn()).mockReturnValue(false);
    expect(new Box(0).filterNot(mock).toJS()).toBe(0);
    expect(mock).toHaveBeenCalledTimes(1);
    expect(mock).toHaveBeenCalledWith(0);

    mock.mockReturnValue(true);
    expect(new Box(1).filterNot(mock).toJS()).toBe();
    expect(mock).toHaveBeenCalledTimes(2);
    expect(mock).toHaveBeenLastCalledWith(1);
  });

  it('should call a function using the box\' value', () => {
    const mock = (jest.fn()).mockReturnValue(1);
    expect(new Box(0).forEach(mock).toJS()).toBe(0);
    expect(mock).toHaveBeenCalledTimes(1);
    expect(mock).toHaveBeenCalledWith(0);
  });

  it('should check if the box is empty', () => {
    expect((new Box()).isEmpty()).toBe(true);
    expect((new Box(0)).isEmpty()).toBe(false);
    expect((new Box('test')).isEmpty()).toBe(false);
  });

  it('should create a mutable box', () => {
    const box = new Box(0, true);
    expect(box.set(1).toJS()).toBe(1);
    expect(box.toJS()).toBe(1);
  });

  it('should make the box mutable & immutable again', () => {
    const box = new Box(0),
      mBox = box.asMutable();

    // Check value after conversion to mutable
    expect(mBox.toJS()).toBe(0);

    // Test mutability/immutability
    box.set(1);
    mBox.set(1);
    expect(box.toJS()).toBe(0);
    expect(mBox.toJS()).toBe(1);

    // Check value after conversion to immutable
    expect(mBox.asImmutable().toJS()).toBe(1);

    // Test immutability after reconversion
    expect(mBox.asImmutable().set(2).toJS()).toBe(2);
    expect(mBox.toJS()).toBe(1);
  });

  it('should perform a mutating operation & return an immutable box', () => {
    const mock =
      jest.fn((b) => {
        // Perform operation requiring mutable box
        b.set(1);
        return b;
      }),
      box = (new Box(0)).withMutations(mock);

    // Test success of mutation & immutability
    box.set(2);
    expect(box.toJS()).toBe(1);
    expect(mock).toHaveBeenCalledTimes(1);
  });

  it('should be the box\' size', () => {
    expect((new Box()).size).toBe(0);
    expect((new Box(0)).size).toBe(1);
  });
});
