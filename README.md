# Immutable Box [![wercker status](https://app.wercker.com/status/5d14d5cef405f8490c3bdfe2d66373aa/s/master "wercker status")](https://app.wercker.com/project/byKey/5d14d5cef405f8490c3bdfe2d66373aa)

> A module that provides a simple immutable box. Use it to establish a powerful linear data flow in JavaScript. (As seen in [this lovely Egghead course](https://egghead.io/courses/professor-frisby-introduces-function-composition "Professor Frisby Introduces Composable Functional JavaScript").)
>
> **[API Reference](https://bitbucket.org/malpaux/immutable-box/wiki/Home)**


## Installing / Getting Started

Install the package
```shell
npm install --save immutable-box
```

and import/require it
```javascript
import Box from 'immutable-box';
// OR (pre ES6)
var Box = require('immutable-box');
```

### Usage
```javascript
Box('value') // put 'value' in a box
  .map((v) => `new ${v}`) // update the box' value
  .get(); // return the box' value (here: 'new value')

Box('1')
  .asMutable() // make the box mutable
  .map((v) => parseInt(v, 10))
  .map((v) => v + 1)
  .includes(2); // compare the box' value (returns true)
```

## Developing

This is what you do after you have cloned the repository:

```shell
npm install
npm run build
```

(Install dependencies & build the project.)

### Linting

Execute ESLint

```shell
npm run lint
```

Try to automatically fix linting errors
```shell
npm run lint:fix
```

### Testing

Execute Jest unit tests using

```shell
npm test
```

Tests are defined in the same directory the module lives in. They are specified in '[module].test.js' files.

### Building

To build the project, execute

```shell
npm run build
```

This saves the production ready code into 'dist/'.

### Documentation

The app is documented using JSDoc. To generate docs, use

```shell
npm run docs
```

This saves HTML documentation into 'docs/'. It requires that you have additionally installed ```jsdoc```.


To generate a Markdown API reference, you can alternatively use

```shell
npm run docs-md
```


This saves the documentation into 'docs/index.md'. It requires that you have additionally installed ```jsdoc-to-markdown```.
