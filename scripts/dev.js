process.env.NODE_ENV = 'development';

var rimrafSync = require('rimraf').sync,
    clearConsole = require('react-dev-utils/clearConsole'),
    formatWebpackMessages = require('react-dev-utils/formatWebpackMessages'),
    webpack = require('webpack'),
    config = require('../config/webpack.config.js'),
    paths = require('../config/paths.js');

function setupCompiler() {
  var compiler = webpack(config);

  compiler.plugin('invalid', function() {
    clearConsole();
    console.log('Building...');
  });

  compiler.plugin('done', function(stats) {
    clearConsole();

    var messages = formatWebpackMessages(stats.toJson({}, true));
    if (!messages.errors.length && !messages.warnings.length) {
      console.log('Built successfully!');
      console.log();
    }

    if (messages.errors.length) {
      console.log('Failed to build.');
      console.log();
      messages.errors.forEach(function (message) {
        console.log(message);
        console.log();
      });
      return;
    }

    if (messages.warnings.length) {
      console.log('Built with warnings.');
      console.log();
      messages.warnings.forEach(function (message) {
        console.log(message);
        console.log();
      });
    }
  });

  return compiler;
}

function run() {
  rimrafSync(paths.appBuild + '/*');
  setupCompiler().watch({
    aggregateTimeout: 300,
    poll: true
  }, function () {});
}

run();
